/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database_systems_ca2;
//STEP 1. Import required packages

import java.sql.*;
import java.util.Scanner;

public class Main_statements {
    // JDBC driver name and database URL

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/database_systems_ca2";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "";

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Connection conn = null;
        Statement stmt = null;
        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to a selected database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");

            //STEP 4: Execute a query
            int switchOpt = 0; //Check user input for quitting file
            while (switchOpt != 16) { //Runs until user enters specific number
                stmt = conn.createStatement();
                //User selects a number below and will go to the Case: of that number I.E: Entering 7 will bring you to Case 7: {see below}
                System.out.println("\nSelect an option:"
                        + "\n1: View customers"
                        + "\n2: View orders"
                        + "\n3: View order details"
                        + "\n4: View games"
                        + "\n5: View distributors"
                        + "\n6: View game distributors"
                        + "\n7: Insert into customers [User-Defined]" //customer_id, customer_name, email_address, zip_code
                        + "\n8: Insert into orders [User-Defined]" //order_id, cutomer_id, order_date
                        + "\n9: Insert into order details [User-Defined]" //order_id, game_id, distributor_id, quantity
                        + "\n10: Insert into games [User-Defined]" //game_id, game_name, genre
                        + "\n11: Insert into distributors [User-Defined]" //distributor_id, distributor_name, location
                        + "\n12: Insert into game distributor [User-Defined]" //game_id, distributor_id, buy_price
                        + "\n13: Update Prepared Statment"
                        + "\n14: Select Prepared Statment"
                        + "\n15: Delete Prepared Statment"
                        + "\n16: Log Out");

                System.out.print("\nSelect a number: ");
                switchOpt = in.nextInt();
                String sql;

                String customer_name;
                String email_address;
                String zip_code;
                int customer_id;
                String order_date;
                int order_id;
                int game_id;
                String distributor_id;
                int quantity;
                String game_name;
                String genre;
                String distributor_name;
                String location;
                double buy_price;
                int row;

                //Cases are selected by users input from above
                switch (switchOpt) {

                    case 1: //View customers
                        System.out.println("Creating statement...");
                        sql = "SELECT * FROM customers";
                        ResultSet rs = stmt.executeQuery(sql);
                        
                        int total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            customer_id = rs.getInt("customer_id");
                            customer_name = rs.getString("customer_name");
                            email_address = rs.getString("email_address");
                            zip_code = rs.getString("zip_code");
                            //Display values
                            System.out.print("ID: " + customer_id);
                            System.out.print(", Name: " + customer_name);
                            System.out.print(", Email Address: " + email_address);
                            System.out.println(", Zip Code: " + zip_code);
                            //count rows
                            total++;

                        }
                        System.out.println("\n" + total + " row's found...");
                        rs.close();
                        stmt.close();
                        break;
                    case 2: //View orders
                        System.out.println("Creating statement...");
                        sql = "SELECT * FROM orders";
                        rs = stmt.executeQuery(sql);

                        total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            order_id = rs.getInt("order_id");
                            customer_id = rs.getInt("customer_id");
                            order_date = rs.getString("order_date");
                            //Display values
                            System.out.print("Order ID: " + order_id);
                            System.out.print(", Customer ID: " + customer_id);
                            System.out.println(", Order Date: " + order_date);
                            //count rows
                            total++;

                        }
                        System.out.println("\n" + total + " row's found...");
                        rs.close();
                        stmt.close();
                        break;
                    case 3: //View order details
                        System.out.println("Creating statement...");
                        sql = "SELECT * FROM order_details";
                        rs = stmt.executeQuery(sql);

                        total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            order_id = rs.getInt("order_id");
                            game_id = rs.getInt("game_id");
                            distributor_id = rs.getString("distributor_id");
                            quantity = rs.getInt("quantity");
                            //Display values
                            System.out.print("Order ID: " + order_id);
                            System.out.print(", Game ID: " + game_id);
                            System.out.print(", Distributor ID: " + distributor_id);
                            System.out.println(", Quantity: " + quantity);
                            //count rows
                            total++;

                        }
                        System.out.println("\n" + total + " row's found...");
                        rs.close();
                        stmt.close();
                        break;
                    case 4: //View games
                        System.out.println("Creating statement...");
                        sql = "SELECT * FROM games";
                        rs = stmt.executeQuery(sql);

                        total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            game_id = rs.getInt("game_id");
                            game_name = rs.getString("game_name");
                            genre = rs.getString("genre");
                            //Display values
                            System.out.print("ID: " + game_id);
                            System.out.print(", Name: " + game_name);
                            System.out.println(", Genre: " + genre);
                            //count rows
                            total++;

                        }
                        System.out.println("\n" + total + " row's found...");
                        rs.close();
                        stmt.close();
                        break;
                    case 5:
                        System.out.println("Creating statement...");
                        sql = "SELECT * FROM distributors";
                        rs = stmt.executeQuery(sql);

                        total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            distributor_id = rs.getString("distributor_id");
                            distributor_name = rs.getString("distributor_name");
                            location = rs.getString("location");
                            //Display values
                            System.out.print("ID: " + distributor_id);
                            System.out.print(", Name: " + distributor_name);
                            System.out.println(", Location: " + location);
                            //count rows
                            total++;

                        }
                        System.out.println("\n" + total + " row's found...");
                        rs.close();
                        stmt.close();
                        break;
                    case 6:
                        System.out.println("Creating statement...");
                        sql = "SELECT * FROM game_distributor";
                        rs = stmt.executeQuery(sql);

                        total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            game_id = rs.getInt("game_id");
                            distributor_id = rs.getString("distributor_id");
                            buy_price = rs.getInt("buy_price");
                            //Display values
                            System.out.print("Game ID: " + game_id);
                            System.out.print(", Distributor ID: " + distributor_id);
                            System.out.println(", Price: " + buy_price);
                            //count rows
                            total++;

                        }
                        System.out.println("\n" + total + " row's found...");
                        rs.close();
                        stmt.close();
                        break;
                    case 7: //Insert into customers
                        System.out.println("INSERTING INTO CUSTOMERS");
                        in.nextLine();
                        System.out.print("Customer Name: ");
                        customer_name = in.nextLine();
                        System.out.print("Email Address: ");
                        email_address = in.nextLine();
                        System.out.print("Zip Code: ");
                        zip_code = in.nextLine();

                        sql = "INSERT INTO customers (customer_name, email_address, zip_code)"
                                + "VALUES ('" + customer_name + "', '" + email_address + "', '" + zip_code + "')";
                        stmt.executeUpdate(sql);
                        System.out.println("Insert successfully created...\n");
                        // Now you can extract all the records
                        // to see the selected records
                        sql = "SELECT * FROM customers";
                        rs = stmt.executeQuery(sql);

                        total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            customer_id = rs.getInt("customer_id");
                            customer_name = rs.getString("customer_name");
                            email_address = rs.getString("email_address");
                            zip_code = rs.getString("zip_code");
                            //Display values
                            System.out.print("ID: " + customer_id);
                            System.out.print(", Name: " + customer_name);
                            System.out.print(", Email Address: " + email_address);
                            System.out.println(", Zip Code: " + zip_code);
                            //count rows
                            total++;

                        }
                        System.out.println("\n" + total + " row's found...");
                        rs.close();
                        stmt.close();
                        break;
                    case 8: //insert into orders
                        System.out.println("INSERTING INTO ORDERS");
                        in.nextLine();
                        System.out.print("Customer ID: ");
                        customer_id = in.nextInt();
                        in.nextLine();
                        System.out.print("Date of Order (YY-MM-DD): ");
                        order_date = in.nextLine();

                        sql = "INSERT INTO orders (customer_id, order_date)"
                                + "VALUES ('" + customer_id + "', '" + order_date + "')";
                        stmt.executeUpdate(sql);
                        System.out.println("Insert successfully created...\n");
                        // Now you can extract all the records
                        // to see the selected records
                        sql = "SELECT * FROM orders";
                        rs = stmt.executeQuery(sql);

                        total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            order_id = rs.getInt("order_id");
                            customer_id = rs.getInt("customer_id");
                            order_date = rs.getString("order_date");
                            //Display values
                            System.out.print("Order ID: " + order_id);
                            System.out.print(", Customer ID: " + customer_id);
                            System.out.println(", Order Date: " + order_date);
                            //count rows
                            total++;

                        }
                        System.out.println("\n" + total + " row's found...");
                        rs.close();
                        stmt.close();
                        break;
                    case 9: //insert into order details
                        System.out.println("INSERTING INTO ORDER DETAILS");
                        in.nextLine();
                        System.out.print("Order ID: ");
                        order_id = in.nextInt();
                        System.out.print("Game ID: ");
                        game_id = in.nextInt();
                        in.nextLine();
                        System.out.print("Distributor ID: ");
                        distributor_id = in.nextLine();
                        System.out.print("Quantity: ");
                        quantity = in.nextInt();

                        sql = "INSERT INTO order_details (order_id, game_id, distributor_id, quantity)"
                                + "VALUES (" + order_id + ", " + game_id + ", '" + distributor_id + "', " + quantity + ")";
                        stmt.executeUpdate(sql);
                        System.out.println("Insert successfully created...\n");
                        // Now you can extract all the records
                        // to see the selected records
                        sql = "SELECT * FROM order_details";
                        rs = stmt.executeQuery(sql);

                        total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            order_id = rs.getInt("order_id");
                            game_id = rs.getInt("game_id");
                            distributor_id = rs.getString("distributor_id");
                            quantity = rs.getInt("quantity");
                            //Display values
                            System.out.print("Order ID: " + order_id);
                            System.out.print(", Game ID: " + game_id);
                            System.out.print(", Distributor ID: " + distributor_id);
                            System.out.println(", Quantity: " + quantity);
                            //count rows
                            total++;

                        }
                        System.out.println("\n" + total + " row's found...");
                        rs.close();
                        stmt.close();
                        break;
                    case 10: //insert into games
                        System.out.println("INSERTING INTO GAMES");
                        in.nextLine();
                        System.out.print("Game Name: ");
                        game_name = in.nextLine();
                        System.out.print("Genre: ");
                        genre = in.nextLine();

                        sql = "INSERT INTO games (game_name, genre)"
                                + "VALUES ('" + game_name + "', '" + genre + "')";
                        stmt.executeUpdate(sql);
                        System.out.println("Insert successfully created...\n");
                        // Now you can extract all the records
                        // to see the selected records
                        sql = "SELECT * FROM games";
                        rs = stmt.executeQuery(sql);

                        total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            game_id = rs.getInt("game_id");
                            game_name = rs.getString("game_name");
                            genre = rs.getString("genre");
                            //Display values
                            System.out.print("ID: " + game_id);
                            System.out.print(", Name: " + game_name);
                            System.out.println(", Genre: " + genre);
                            //count rows
                            total++;

                        }
                        System.out.println("\n" + total + " row's found...");
                        rs.close();
                        stmt.close();
                        break;
                    case 11: //insert into distributors
                        System.out.println("INSERTING INTO DISTRIBUTORS");
                        in.nextLine();
                        System.out.print("Distributor ID: ");
                        distributor_id = in.nextLine();
                        System.out.print("Distributor Name: ");
                        distributor_name = in.nextLine();
                        System.out.print("Location: ");
                        location = in.nextLine();

                        sql = "INSERT INTO distributors (distributor_id, distributor_name, location)"
                                + "VALUES ('" + distributor_id + "', '" + distributor_name + "', '" + location + "')";
                        stmt.executeUpdate(sql);
                        System.out.println("Insert successfully created...\n");
                        // Now you can extract all the records
                        // to see the selected records
                        sql = "SELECT * FROM distributors";
                        rs = stmt.executeQuery(sql);

                        total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            distributor_id = rs.getString("distributor_id");
                            distributor_name = rs.getString("distributor_name");
                            location = rs.getString("location");
                            //Display values
                            System.out.print("ID: " + distributor_id);
                            System.out.print(", Name: " + distributor_name);
                            System.out.println(", Location: " + location);
                            //count rows
                            total++;

                        }
                        System.out.println("\n" + total + " row's found...");
                        rs.close();
                        stmt.close();
                        break;
                    case 12: //insert into game distributor
                        System.out.println("INSERTING INTO GAME DISTRIBUTOR");
                        in.nextLine();
                        System.out.print("Game ID: ");
                        game_id = in.nextInt();
                        in.nextLine();
                        System.out.print("Distributor ID: ");
                        distributor_id = in.nextLine();
                        System.out.print("Buy Price: ");
                        buy_price = in.nextDouble();
                        //STEP 4: Execute a query
                        sql = "INSERT INTO game_distributor (game_id, distributor_id, buy_price)"
                                + "VALUES (" + game_id + ", '" + distributor_id + "', '" + buy_price + "')";
                        stmt.executeUpdate(sql);
                        System.out.println("Insert successfully created...\n");
                        // Now you can extract all the records
                        // to see the selected records
                        sql = "SELECT * FROM game_distributor";
                        rs = stmt.executeQuery(sql);

                        total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            game_id = rs.getInt("game_id");
                            distributor_id = rs.getString("distributor_id");
                            buy_price = rs.getInt("buy_price");
                            //Display values
                            System.out.print("Game ID: " + game_id);
                            System.out.print(", Distributor ID: " + distributor_id);
                            System.out.println(", Price: " + buy_price);
                            //count rows
                            total++;

                        }
                        System.out.println("\n" + total + " row's found...");
                        rs.close();
                        stmt.close();
                        break;
                    case 13: //Update statement
                        System.out.println("UPDATE PREPARED STATEMENT 1");
                        System.out.println("Creating statement...");
                        stmt = conn.createStatement();
                        sql = "UPDATE customers "
                                + "SET email_address = 'TESTINGUPDATE@EMAIL.COM' WHERE customer_name = 'Jordan Dowd'";
                        row = stmt.executeUpdate(sql);
                        if (row > 1) {
                            System.out.println("\n" + row + " row's updated...\n");
                        } else {
                            System.out.println("\n" + row + " row updated...\n");
                        }

                        // Now you can extract all the records
                        // to see the updated records
                        sql = "SELECT * FROM customers";
                        rs = stmt.executeQuery(sql);

                        total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            customer_id = rs.getInt("customer_id");
                            customer_name = rs.getString("customer_name");
                            email_address = rs.getString("email_address");
                            zip_code = rs.getString("zip_code");
                            //Display values
                            System.out.print("ID: " + customer_id);
                            System.out.print(", Name: " + customer_name);
                            System.out.print(", Email Address: " + email_address);
                            System.out.println(", Zip Code: " + zip_code);
                            //count rows
                            total++;

                        }
                        System.out.println("\n" + total + " row's found...");
                        rs.close();
                        stmt.close();
                        break;
                    case 14: //select statement
                        System.out.println("SELECT PREPARED STATEMENT 1");
                        System.out.println("Creating statement...");
                        stmt = conn.createStatement();
                        sql = "SELECT d.distributor_id, g.game_name, gd.buy_price "
                                + "FROM distributors d, games g, game_distributor gd "
                                + "WHERE d.distributor_id = gd.distributor_id "
                                + "AND g.game_id = gd.game_id "
                                + "AND d.distributor_id = 'LWOODS'";
                        rs = stmt.executeQuery(sql);

                        total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            distributor_id = rs.getString("distributor_id");
                            game_name = rs.getString("game_name");
                            buy_price = rs.getInt("buy_price");
                            //Display values
                            System.out.print("ID: " + distributor_id);
                            System.out.print(", Game: " + game_name);
                            System.out.println(", Price: " + buy_price);
                            //Counts rows
                            total++;

                        }
                        if (total > 1) {
                            System.out.println("\n" + total + " row's selected...\n");
                        } else {
                            System.out.println("\n" + total + " row selected...\n");
                        }
                        rs.close();
                        stmt.close();
                        break;
                    case 15:
                        System.out.println("Creating statement...");
                        stmt = conn.createStatement();
                        sql = "DELETE FROM orders "
                                + "WHERE customer_id = 1";
                        row = stmt.executeUpdate(sql);
                        if (row > 1) {
                            System.out.println("\n" + row + " row's deleted...\n");
                        } else {
                            System.out.println("\n" + row + " row deleted...\n");
                        }
                        // Now you can extract all the records
                        // to see the remaining records
                        sql = "SELECT * FROM orders";
                        rs = stmt.executeQuery(sql);

                        total = 0;
                        //STEP 5: Extract data from result set
                        while (rs.next()) {
                            //Retrieve by column name
                            customer_id = rs.getInt("customer_id");
                            order_id = rs.getInt("order_id");
                            order_date = rs.getString("order_date");

                            //Display values
                            System.out.print("Customer ID: " + customer_id);
                            System.out.print(", Order ID: " + order_id);
                            System.out.println(", Order Date: " + order_date);
                            //count rows 
                            total++;
                        }
                        if (total > 1) {
                            System.out.println("\n" + total + " row's selected...\n");
                        } else {
                            System.out.println("\n" + total + " row selected...\n");
                        }
                        rs.close();
                        stmt.close();
                        break;
                }
            }
            //STEP 6: Clean-up environment
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) {
                    conn.close();
                }
            } catch (SQLException se) {
            }// do nothing
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");
    }//end main
}//end JDBCExample
