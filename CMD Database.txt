
cd c:\xampp
xampp_start	
cd mysql
cd bin

mysql -uroot -p
mysql

use test;

create table customers ( 
customer_id int NOT NULL AUTO_INCREMENT,
customer_name varchar(20),
email_address varchar(30),
zip_code varchar(20),
PRIMARY KEY (customer_id));

create table distributors (
distributor_id varchar(20),
distributor_name varchar(20),
location varchar(30),
PRIMARY KEY (distributor_id));

create table orders ( 
order_id int NOT NULL AUTO_INCREMENT,
customer_id int NOT NULL,
order_date date ,
PRIMARY KEY (order_id),
FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
ON DELETE CASCADE
ON UPDATE CASCADE);

create table games ( 
game_id int NOT NULL AUTO_INCREMENT,
game_name varchar(40),
genre varchar(20),
PRIMARY KEY (game_id));

create table order_details (
order_id int NOT NULL,
game_id int NOT NULL,
distributor_id varchar(20),
quantity int,
PRIMARY KEY (order_id, game_id),
FOREIGN KEY (order_id) REFERENCES orders(order_id)
ON DELETE CASCADE
ON UPDATE CASCADE, 
FOREIGN KEY (game_id) REFERENCES games(game_id)
ON DELETE CASCADE
ON UPDATE CASCADE,
FOREIGN KEY (distributor_id) REFERENCES distributors(distributor_id)
ON DELETE CASCADE
ON UPDATE CASCADE);

create table game_distributor (
game_id int NOT NULL,
distributor_id varchar(20),
buy_price decimal(10,2),
PRIMARY KEY (game_id, distributor_id),
FOREIGN KEY (game_id) REFERENCES games(game_id)
ON DELETE CASCADE
ON UPDATE CASCADE,
FOREIGN KEY (distributor_id) REFERENCES distributors(distributor_id)
ON DELETE CASCADE
ON UPDATE CASCADE);

insert into customers values (1, "Jordan Dowd", "jordand@gmail.com", "A23 345");
insert into customers values (2, "Alex Jevdokimov", "alex96@yahoo.com", "H34 654");
insert into customers values (3, "Conor Fahey", "conorfahey7@yahoo.ie", "Z54 974");
insert into customers values (4, "Matthew Kelly", "matthew122@gmail.ie", "B23 567");
insert into customers values (5, "Shannon Boyle", "shannonboyle@yahoo.co.uk", "J34 932");
insert into customers values (6, "Kevin Gerrard", "kev2702@gmail.com", "K23 836");

insert into distributors values ("AZON", "Amazon Inc.", "America/England");
insert into distributors values ("GTREE", "Gum Tree", "America/Europe");
insert into distributors values ("GWORLD", "Game World", "Europe");
insert into distributors values ("LWOODS", "Little Woods", "Ireland");

insert into orders values (1, 1, "2001-09-01");
insert into orders values (2, 3, "2001-09-18");
insert into orders values (3, 4, "2001-09-26");
insert into orders values (4, 5, "2001-10-03");
insert into orders values (5, 2, "2001-11-26");

insert into games values (1, "For Honor", "Action");
insert into games values (2, "Watch Dogs 2", "Action/Adventure");
insert into games values (3, "Tom Clancy's The Divison", "Action/Adventure");
insert into games values (4, "Far Cry Primal", "Action/Adventure");
insert into games values (5, "Steep", "Extreme Sports");
insert into games values (6, "Uncharted 4: A Thief's End", "Action/Adventure");
insert into games values (7, "The Last Of Us", "Survival Horror");
insert into games values (8, "Uncharted: The Collection", "Action/Adventure");
insert into games values (9, "Mass Effect: Andromeda", "Action/Adventure");
insert into games values (10, "Battlefield 1", "FPS");
insert into games values (11, "Titanfall 2", "FPS");
insert into games values (12, "Fifa 17", "Sports");
insert into games values (13, "EA Sports UFC 2", "Extreme Sports");
insert into games values (14, "Mirror's Edge", "Action/Role Playing");
insert into games values (15, "Crash Bandicoot Remastered", "Party/Platform");
insert into games values (16, "Call of Duty: IW", "FPS");
insert into games values (17, "Destiny: Rise of Iron", "Action/Role Playing");
insert into games values (18, "Skylanders Imaginators", "Platform/Role Playing");

insert into order_details values (1, 1, "LWOODS", 1);
insert into order_details values (1, 7, "GWORLD", 2);
insert into order_details values (1, 12, "GWORLD", 1);
insert into order_details values (2, 16, "GTREE", 1);
insert into order_details values (2, 18, "AZON", 1);
insert into order_details values (2, 6, "AZON", 1);
insert into order_details values (2, 7, "GWORLD", 1);
insert into order_details values (3, 4, "LWOODS", 1);
insert into order_details values (3, 17, "AZON", 1);
insert into order_details values (4, 1, "AZON", 1);
insert into order_details values (4, 10, "GTREE", 2);
insert into order_details values (4, 9, "AZON", 1);
insert into order_details values (4, 6, "AZON", 1);
insert into order_details values (4, 5, "GWORLD", 1);
insert into order_details values (5, 3, "AZON", 2);
insert into order_details values (5, 15, "LWOODS", 1);

insert into game_distributor values (1, "AZON", 34.99);
insert into game_distributor values (1, "GTREE", 42.99);
insert into game_distributor values (1, "GWORLD", 37.99);
insert into game_distributor values (1, "LWOODS", 35.99);
insert into game_distributor values (2, "AZON", 55.99);
insert into game_distributor values (2, "GWORLD", 54.99);
insert into game_distributor values (3, "AZON", 50.99);
insert into game_distributor values (3, "GTREE", 47.99);
insert into game_distributor values (3, "GWORLD", 45.99);
insert into game_distributor values (4, "AZON", 34.99);
insert into game_distributor values (4, "LWOODS", 37.99);
insert into game_distributor values (5, "AZON", 27.99);
insert into game_distributor values (5, "GTREE", 18.00);
insert into game_distributor values (5, "GWORLD", 25.95);
insert into game_distributor values (6, "AZON", 34.99);
insert into game_distributor values (7, "GTREE", 29.99);
insert into game_distributor values (7, "GWORLD", 32.00);
insert into game_distributor values (7, "LWOODS", 28.00);
insert into game_distributor values (8, "GWORLD", 32.99);
insert into game_distributor values (8, "LWOODS", 30.99);
insert into game_distributor values (9, "AZON", 28.99);
insert into game_distributor values (9, "LWOODS", 34.99);
insert into game_distributor values (10, "AZON", 55.99);
insert into game_distributor values (10, "GTREE", 54.99);
insert into game_distributor values (10, "GWORLD", 55.99);
insert into game_distributor values (10, "LWOODS", 53.99);
insert into game_distributor values (11, "AZON", 28.99);
insert into game_distributor values (11, "LWOODS", 27.99);
insert into game_distributor values (12, "AZON", 21.00);
insert into game_distributor values (12, "GTREE", 22.00);
insert into game_distributor values (13, "GTREE", 34.00);
insert into game_distributor values (13, "GWORLD", 32.00);
insert into game_distributor values (13, "LWOODS", 32.00);
insert into game_distributor values (14, "AZON", 29.99);
insert into game_distributor values (14, "GWORLD", 32.99);
insert into game_distributor values (15, "AZON", 15.00);
insert into game_distributor values (15, "LWOODS", 20.00);
insert into game_distributor values (16, "GTREE", 32.00);
insert into game_distributor values (16, "GWORLD", 31.00);
insert into game_distributor values (17, "AZON", 55.00);
insert into game_distributor values (17, "GTREE", 55.00);
insert into game_distributor values (18, "AZON", 34.00);
insert into game_distributor values (18, "GWORLD", 33.99);
insert into game_distributor values (18, "LWOODS", 34.99);




